# Using Debian Stretch Docker Images

Welcome to the jhsu802701 repository for using Docker images based on Debian Stretch.

## Prerequisites
Go to https://gitlab.com/rubyonracetracks/docker-debian-stretch-use for more details.

## Other Notes
Go to https://gitlab.com/rubyonracetracks/docker-debian-stretch-use for more details.

## Using Docker Images
Go to https://gitlab.com/rubyonracetracks/docker-debian-stretch-use for more details.

## Docker Images Available
| Script | Docker Image | Comments |
|--------|--------------|----------|
| rbenv-rails-sessionizer.sh | [debian-stretch-rbenv-rails-sessionizer](https://gitlab.com/jhsu802701/docker-debian-stretch-rvm-rails-sessionizer) | For working on the Minnestar Sessionizer app |
| rbenv-rails-codetriage.sh | [debian-stretch-rbenv-rails-codetriage](https://gitlab.com/jhsu802701/docker-debian-stretch-rvm-rails-codetriage) | For working on the CodeTriage app |
| rbenv-rails-bridge_troll.sh | [debian-stretch-rbenv-rails-bridge_troll](https://gitlab.com/jhsu802701/docker-debian-stretch-rvm-rails-bridge_troll) | For working on the Bridge Troll app |
